import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from "./heroes/dashboard/dashboard.component";
import { HeroDetailsComponent } from "./heroes/hero-details/hero-details.component";
import { HeroListComponent } from "./heroes/hero-list/hero-list.component";

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {
    path: 'detail/:id',
    loadChildren: () => import('./heroes/hero-details/hero-details.module').then(m => m.HeroDetailsModule)
  },
  {path: 'create', component: HeroDetailsComponent},
  {path: 'heroes', component: HeroListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
