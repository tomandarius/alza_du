import {Injectable} from '@angular/core';
import {Hero} from "../type";

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private heroes: Hero[] = [
    {id: 1, name: 'Dr Nice', description: ''},
    {id: 2, name: 'Narco', description: ''},
    {id: 3, name: 'Bombasto', description: ''},
    {id: 4, name: 'Celeritas', description: ''},
    {id: 5, name: 'Magneta', description: ''},
    {id: 6, name: 'RubberMan', description: ''},
    {id: 7, name: 'Dynama', description: ''},
    {id: 8, name: 'Dr IQ', description: ''},
    {id: 9, name: 'Magma', description: ''},
    {id: 10, name: 'Tornado', description: ''}
  ];

  constructor() {
  }

  private get getId(): number {
    return this.heroes.length > 0 ? Math.max(...this.heroes.map(h => h.id!)) + 1 : 1;
  }

  public get allHeroes(): Hero[] {
    return [...this.heroes];
  }

  public createHero(hero: Hero): void {
    hero.id = this.getId;
    this.heroes.push(hero);
  }

  public updateHero(hero: Hero): void {
    let index = this.heroes.findIndex(h => h.id == hero.id);
    if (index != -1) {
      this.heroes[index] = hero;
    }
  }

  public getDetails(id: number): Hero | undefined {
    return this.heroes.find(h => h.id == id);
  }

  public deleteHero(id: number): void {
    this.heroes = this.heroes.filter(h => h.id != id);
  }
}
