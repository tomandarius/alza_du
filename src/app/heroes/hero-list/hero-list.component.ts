import { Component, OnInit } from '@angular/core';
import { HeroService } from "../service/hero.service";
import { Hero } from "../type";
import { Router } from "@angular/router";

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.scss']
})
export class HeroListComponent implements OnInit {
  public heroes: Hero[] = [];
  public selectedHero: Hero | undefined;

  constructor(
    private heroService: HeroService,
    private router: Router
  ) {
    this.heroes = heroService.allHeroes;
  }

  ngOnInit(): void {
  }

  onDetails(): void {
    if (this.selectedHero) {
      this.router.navigate(['detail', this.selectedHero.id])
    }
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  onDelete(id: number): void {
    this.heroService.deleteHero(id);
    this.heroes = this.heroService.allHeroes;
  }
}
