﻿import { NgModule } from "@angular/core";
import { AppComponent } from "../../app.component";
import { HeroDetailsComponent } from "./hero-details.component";
import { ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../../material/material.module";
import { CommonModule } from "@angular/common";
import { FlexLayoutModule } from "@angular/flex-layout";
import { RouterModule } from "@angular/router";


@NgModule({
  declarations: [
    HeroDetailsComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MaterialModule,
    RouterModule.forChild([{path: "", component: HeroDetailsComponent}])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class HeroDetailsModule {
}
