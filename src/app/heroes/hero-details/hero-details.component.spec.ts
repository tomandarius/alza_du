import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroService } from '../service/hero.service';

import { HeroDetailsComponent } from './hero-details.component';

describe('HeroDetailsComponent', () => {
  let component: HeroDetailsComponent;
  let service = new HeroService();
  let fixture: ComponentFixture<HeroDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [HeroDetailsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should check if hero name is contain in list', () => {
    const name = "Narco";

    expect(component).toBeTruthy();
    expect(
      service.allHeroes.map(h => h.name)
    ).toContain(name);
  });

  it('should check if hero name is not contain in list', () => {
    const name = "Darius";

    expect(component).toBeTruthy();
    expect(
      service.allHeroes.map(h => h.name)
    ).not.toContain(name);
  });
});
