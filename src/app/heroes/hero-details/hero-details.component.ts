import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { HeroService } from "../service/hero.service";
import { Hero } from "../type";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-hero-details',
  templateUrl: './hero-details.component.html',
  styleUrls: ['./hero-details.component.scss']
})
export class HeroDetailsComponent implements OnInit {
  public hero: Hero | undefined;
  public form: FormGroup;
  public isNew = false;

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) {
    this.form = new FormGroup({
        id: new FormControl(''),
        name: new FormControl('', Validators.required),
        description: new FormControl(''),
      }
    );
  }

  public ngOnInit(): void {
    this.getHero();
  }

  private getHero(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id') as string, 10);
    if (id) {
      this.hero = this.heroService.getDetails(id)!;
      this.form.patchValue(this.hero)
      this.form.get('id')?.disable();
    } else {
      this.isNew = true;
      this.form.removeControl('id');
    }
  }

  public onSave(): void {
    if (this.form.valid) {

      this.hero = {
        ...this.hero,
        ...this.form.value
      };

      if (this.isNew) {
        this.heroService.createHero(this.hero!);
      } else {
        this.heroService.updateHero(this.hero!);
      }
      this.goBack();
    }
  }

  goBack(): void {
    this.location.back();
  }

  get getTitle(): string {
    const name = this.form.get('name')?.value.toUpperCase();
    return this.isNew ? 'New Hero: ' + name : name + ' details!';
  }
}
