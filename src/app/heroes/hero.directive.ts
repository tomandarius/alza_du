import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHero]'
})
export class HeroDirective {
  @Input('appHero') name: string | undefined;

  constructor(private elementRef: ElementRef, private render: Renderer2) {
  }

  ngOnInit() {
    if (this.name && this.name.length > 9) {
      this.render.setStyle(this.elementRef.nativeElement, 'font-weight', 'bold');
      this.render.setStyle(this.elementRef.nativeElement, 'color', 'blue');
    }
  }
}
