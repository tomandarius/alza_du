import { Component, OnInit } from '@angular/core';
import { HeroService } from "../service/hero.service";
import { Hero } from "../type";
import { Router } from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public heroes: Hero[] = [];

  constructor(
    private heroService: HeroService,
    private router: Router
  ) {
    this.heroes = heroService.allHeroes.slice(0, 5);
  }

  ngOnInit(): void {
  }


  onDetails(hero: Hero): void {
    this.router.navigate(['detail', hero.id]);
  }
}
